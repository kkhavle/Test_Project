import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;

public class TestSteps {
    public static WebDriver driver;

    @When("^Navigated to portal$")
    public void navigatedToPortal() throws Throwable {

        driver.get("https://retailer.allpointsportal.com/");

    }

    @Then("^Verify Title$")
    public void verifyTitle() throws Throwable {
        Assert.assertEquals("AllPoints: Retailer",driver.getTitle());

    }
}
