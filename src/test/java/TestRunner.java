import com.saucelabs.common.SauceOnDemandAuthentication;
import com.saucelabs.junit.ConcurrentParameterized;
import com.saucelabs.junit.SauceOnDemandTestWatcher;
import cucumber.api.CucumberOptions;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

@CucumberOptions (
        features = {"src/test/java/Test.Feature"}
)


public class TestRunner extends AbstractTestNGCucumberTests{

    public static final String USERNAME = System.getenv("SAUCE_USERNAME");
    public static final String ACCESS_KEY = System.getenv("SAUCE_ACCESS_KEY");
    public static final String URL = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub";
    public static WebDriver driver;
    public String sessionId;
    public String jobName;
    public static DesiredCapabilities caps;

    @Before
    public void setUp(Scenario scenario) throws Exception {
        caps = new DesiredCapabilities();
        caps.setCapability("platform", System.getProperty("platform"));
        caps.setCapability("browserName", System.getProperty("browserName"));
        caps.setCapability("version", System.getProperty("version"));
        caps.setCapability("screenResolution", System.getProperty("screenResolution"));
        caps.setCapability("suitename", System.getProperty("suitename"));

        String date = new SimpleDateFormat("MM-dd-yyyy").format(new Date());
        jobName = scenario.getName() + "_" + caps.getPlatform() + "_" + caps.getBrowserName() + caps.getVersion();
        caps.setCapability("name", jobName);
        caps.setCapability("build", System.getProperty("suitename") + "_" + date);

        driver = new RemoteWebDriver(new URL(URL), caps);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        sessionId = (((RemoteWebDriver) driver).getSessionId()).toString();
    }

    @After
    public void tearDown(Scenario scenario) throws Exception {
        SauceUtils.UpdateResults(USERNAME, ACCESS_KEY, !scenario.isFailed(), sessionId);
        System.out.println("SauceOnDemandSessionID="+ sessionId + "job-name="+ jobName);
        driver.quit();
    }



}
